package com.boontaran.games.mygame;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.boontaran.MessageListener;
import com.boontaran.games.platformerLib.Entity;
import com.boontaran.games.platformerLib.World;
import com.boontaran.games.mygame.control.CButton;
import com.boontaran.games.mygame.control.JoyStick;
import com.boontaran.games.mygame.mk.ChallengeDialog;
import com.boontaran.games.mygame.mk.EnumAlphabet;
import com.boontaran.games.mygame.panel.CoinCounter;
import com.boontaran.games.mygame.panel.Timer;
import com.boontaran.games.platformerLib.Lands;
import com.boontaran.games.tiled.TileLayer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Level extends World {
    //tmx filename & location

    protected String tmxFile;

    //values based on the tmx file
    private int mapWidth, mapHeight, tilePixelWidth, tilePixelHeight;
    private int levelWidth, levelHeight;

    //level states
    protected static final int GAME_PLAY = 1;
    protected static final int GAME_COMPLETED = 2;
    protected static final int GAME_PAUSED = 3;
    protected int state;

    //key states
    private boolean keyLeft, keyRight, keyJump, keyFire;

    //entities
    protected Person person;

    //level bg
    protected Image levelBg;

    //callback message code
    public static final int FAILED = 1;
    public static final int COMPLETED = 2;

    public int id; //id of level, should be unique..

    //top panel items
//    private HealthBar healthBar;
    private CoinCounter coinCounter;

    private Timer timer;

    //level data
    private int numCoins, numBullets, score;

    //pause dialog
    private PauseDialog pauseDialog;
    private ChallengeDialog challengeDialog;
    private Lands lands;

    private TiledMap map;

    private float i = 1000;

    //on screen virtual button
    private JoyStick joyStick;
    private CButton jumpBtn, bulletBtn;

    //Alphabet
    List<EnumAlphabet> alphabet;
    private EnumAlphabet lastLetter;

    float lastMysteryPos;
    // end pools

    public Level(int id) {
        this.id = id;
        init();
    }

    public void init() {

        tmxFile = "tiled/level" + id + ".tmx";

        //World.debug = true;
        this.alphabet = new ArrayList<EnumAlphabet>();
        this.alphabet.addAll(Arrays.asList(EnumAlphabet.values()));
        //Embaralhamos os números:
//        Collections.shuffle(this.alphabet);

        this.lastLetter = null;
        this.lands = new Lands(this);
    }

    public void setBackgroundRegion(String regionName) {
        levelBg = new Image(MyGame.atlas.findRegion(regionName));
        addBackground(levelBg, true, false);
    }

    @Override
    protected void create() {
        gravity.y = Settings.GRAVITY;
        init();

        if (tmxFile == null) {
            throw new Error("TMX file not defined yet !!!");
        }

        buildLayer();
        if (person == null) {
            throw new Error("hero not defined yet in tmx file !!!");
        }

        //pull hero on top of other objects
        stage.addActor(person);

        //camera follow hero
        camController.followObject(person);
        //camera clamp, make sure it only showing game area
        camController.setBoundary(new Rectangle(0, 0, levelWidth, levelHeight));

        coinCounter = new CoinCounter();
        addOverlayChild(coinCounter);

        coinCounter.setY(getHeight() - coinCounter.getHeight() - 10);
        
        this.timer = new Timer();
        addOverlayChild(this.timer);
        this.timer.start();
        this.timer.setX(coinCounter.getRight() + 50);
        this.timer.setY(coinCounter.getY() + 5);
        //score
        
        //create the on screen control
        float minHeight = mmToPx(10);
        joyStick = new JoyStick(minHeight);
        addOverlayChild(joyStick);
        joyStick.setPosition(15, 15);
        //on screen button
        jumpBtn = new CButton(new Image(MyGame.atlas.findRegion("jump")), new Image(MyGame.atlas.findRegion("jump_down")), minHeight);
        bulletBtn = new CButton(new Image(MyGame.atlas.findRegion("bullet_btn")), new Image(MyGame.atlas.findRegion("bullet_btn_down")), minHeight);

        jumpBtn.setPosition(getWidth() - jumpBtn.getWidth() - 15, 15);
        addOverlayChild(jumpBtn);

        bulletBtn.setPosition(jumpBtn.getX() - bulletBtn.getWidth() - 0.4f * bulletBtn.getWidth(), 15);
        addOverlayChild(bulletBtn);

        //camController.setDefaultZoom(1.5f);
        updatePanel();
        state = GAME_PLAY;
    }

    private void buildLevel() {

        for (MapLayer layer : map.getLayers()) {
            if (layer.getObjects().getCount() > 0) {

                if (layer.getName().equals("fixed")) {
                    //build fixed object : land & platform
                    createFloor(layer.getObjects(), i += 100);
                }
            }
        }

    }

    private void buildLayer() {

        //assign filter to avoid artifact when it resized to match screen size
        TmxMapLoader.Parameters params = new TmxMapLoader.Parameters();
        params.generateMipMaps = true;
        params.textureMinFilter = TextureFilter.MipMapLinearNearest;
        params.textureMagFilter = TextureFilter.Linear;

        //get and calculate map size
        this.map = new TmxMapLoader().load(tmxFile, params);

        MapProperties prop = map.getProperties();
        mapWidth = prop.get("width", Integer.class);
        mapHeight = prop.get("height", Integer.class);
        tilePixelWidth = prop.get("tilewidth", Integer.class);
        tilePixelHeight = prop.get("tileheight", Integer.class);
        levelWidth = mapWidth * tilePixelWidth;
        levelHeight = mapHeight * tilePixelHeight;

        TileLayer tLayer = null;
        int layerId = 0;
        //inspect the layers of tmx files

        for (MapLayer layer : map.getLayers()) {

            if (layer.getObjects().getCount() > 0) {
                tLayer = null;

                String name = layer.getName();
                if (name.equals("fixed")) {
                    //build fixed object : land & platform
                } else if (name.equals("entity")) {
                    //create game entity : hero, enemies, coins, etc
                    createEntities(layer.getObjects());
                }
            } else {
                //tiled layer only, no object
                if (tLayer == null) {
                    tLayer = new TileLayer(camera, map, layerId, stage.getBatch());
                } else {
                    tLayer.addLayerId(layerId);
                }
                addChild(tLayer);
            }
            layerId++;
        }

        //create world boundary wall 
        //left
        Entity wall = new Entity();
        wall.setSize(10, levelHeight);
        wall.setPosition(-5, levelHeight / 2);
        addLand(wall, false);

        //right
        wall = new Entity();
        wall.setSize(10, levelHeight);
        addLand(wall, false);

        //top
        wall = new Entity();
        wall.setSize(levelWidth, 10);
        wall.setPosition(levelWidth / 2, levelHeight + 15);
        addLand(wall, false);

    }

    private void createEntities(MapObjects objects) {
        Rectangle rect;
        Integer i = -1;
        for (MapObject obj : objects) {
            rect = ((RectangleMapObject) obj).getRectangle();
            String name = obj.getName();

            if (name.equals("person")) {
                //create hero
                person = new Person(this);
                person.setPosition(rect.x + rect.width / 2, rect.y + person.getHeight() / 2);
                addEntity(person);

            } else {
                createOtherEntity(obj);
            }
        }
        this.lastMysteryPos = person.getPos().x + 500;

    }

    public void makeMystery(boolean flag) {

        if ((int) person.getPos().x % 5 == 0) {

//            Y
            lastMysteryPos += randon(100, 200);

            MysteryBox box = null;
            if (flag) {
                box = createMystery();
            } else {
                box = createMysteryNext();
            }

            box.setPosition(lastMysteryPos, randon(350, 400));

            //addEntity(box);
            addLandMysteryBox(box, false);
        }
    }

    public MysteryBox createMystery() {
        Collections.shuffle(this.alphabet);

        MysteryBox box = new MysteryBox(this);

        if (!this.alphabet.isEmpty()) {
            box.setValue(this.alphabet.get(0));
            box.getLabel().setText(box.getValue().name());
        }
        return box;
    }

    public MysteryBox createMysteryNext() {
        this.alphabet = new ArrayList<EnumAlphabet>();
        this.alphabet.addAll(Arrays.asList(EnumAlphabet.values()));

        MysteryBox box = new MysteryBox(this);

        if (this.lastLetter == null) {
            box.setValue(EnumAlphabet.A);
            box.getLabel().setText(box.getValue().name());
        } else {
            if (!this.alphabet.isEmpty()) {
                box.setValue(this.alphabet.get(lastLetter.getNumber()));
                box.getLabel().setText(box.getValue().name());
            }
        }

        return box;
    }

    public int randon(int minimo, int maximo) {
        Random random = new Random();
        return random.nextInt((maximo - minimo) + 1) + minimo;
    }

    protected void createOtherEntity(MapObject obj) {
        //sub class to implement
    }

    private void createFloor(MapObjects objects, float width) {
        for (MapObject obj : objects) {
            Rectangle rect = ((RectangleMapObject) obj).getRectangle();

            Entity ent = new Entity();
            ent.setSize(width, rect.height);
            ent.setPosition(person.getX() + rect.width / 2, rect.y + rect.height / 2);
            addLand(ent, true);
        }
    }

    //update the value on panel items
    protected void updatePanel() {
        if (this.lastLetter != null) {
            coinCounter.setCount(this.lastLetter.name());
        } else {
            coinCounter.setCount("-");
        }

        if (numBullets > 0) {
            bulletBtn.setVisible(true);
        } else {
            bulletBtn.setVisible(false);
        }
    }

    public int getBulletCounts() {
        return numBullets;
    }

    //collision handling
    @Override
    public void onCollide(Entity entA, Entity entB, float delta) {

    }

    //key handling
    //store the boolean values based on pressed keys,
    @Override
    public boolean keyDown(int keycode) {

        // on desktop only, while on android use the virtual button
        if (keycode == Keys.LEFT) {
            keyLeft = true;
        }
        if (keycode == Keys.RIGHT) {
            keyRight = true;
        }
        if (keycode == Keys.S || keycode == Keys.UP) {
            keyJump = true;
        }
        if (keycode == Keys.A) {
            keyFire = true;
        }

        if (keycode == Keys.P) {
            if (state == GAME_PAUSED) {
                resumeLevel();
            } else {
                pauseLevel(false);
            }

        }
        if (keycode == Keys.ESCAPE || keycode == Keys.BACK) {  //back key pressed
            if (state == GAME_PAUSED) {
                resumeLevel();
            } else {
                pauseLevel();
            }
            return true;
        }
        return super.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Keys.LEFT) {
            keyLeft = false;
        }
        if (keycode == Keys.RIGHT) {
            keyRight = false;
        }
        if (keycode == Keys.S || keycode == Keys.UP) {
            keyJump = false;
        }
        if (keycode == Keys.A) {
            keyFire = false;
        }
        return super.keyUp(keycode);
    }

    //resume game
    private void resumeLevel() {
        state = GAME_PLAY;
        resumeWorld();

        if (pauseDialog != null) {
            removeOverlayChild(pauseDialog);
        }

        if (challengeDialog != null) {
            removeOverlayChild(challengeDialog);
        }

        MyGame.media.unMuteAllMusic();
        call(MyGame.LEVEL_RESUMED);
    }

    //pause game 
    private void pauseLevel() {
        pauseLevel(true);
    }

    private void pauseLevel(boolean withdialog) {
        if (state != GAME_PLAY) {
            return;
        }

        state = GAME_PAUSED;
        pauseWorld();

        if (!withdialog) {
            return;
        }

        //show the paused dialog
        pauseDialog = new PauseDialog();
        addOverlayChild(pauseDialog);
        centerActorXY(pauseDialog);
        pauseDialog.addListener(new MessageListener() {

            //the dialog callback
            @Override
            protected void receivedMessage(int message, Actor actor) {
                if (actor == pauseDialog) {
                    if (message == PauseDialog.ON_RESUME) {
                        resumeLevel();
                    } else if (message == PauseDialog.ON_QUIT) {
                        call(Level.COMPLETED);
                    }
                }
            }

        });

        MyGame.media.muteAllMusic();
        //track the event
        call(MyGame.LEVEL_PAUSED);
    }

    protected void levelCompleted() {
        //persistent data ...
        //update the level progress
        MyGame.data.setLevelProgress(id);

        //update the score
        MyGame.data.updateScore(id, score);

        //stop camera and pause the loop
        camController.followObject(null);
        pauseWorld();

        //show "level completed" dialog box
        LevelCompletedDialog dialog = new LevelCompletedDialog(score);
        addOverlayChild(dialog);
        centerActorXY(dialog);

        MyGame.media.playSound("level_completed");
        MyGame.media.stopMusic("level");
        call(MyGame.LEVEL_COMPLETED);
    }

    EnumAlphabet next = null;

    int count = 0;
    int count2 = 5;

    //the game loop
    @Override
    protected void update(float delta) {
        count++;
        count2++;
        this.timer.start();
        if (state == GAME_PLAY) {

            //notify hero what keys are pressed
            boolean lKeyRight = keyRight || joyStick.isRight();
            boolean lKeyLeft = keyLeft || joyStick.isLeft();
            boolean lKeyJump = keyJump || jumpBtn.isPressed();
            boolean lKeyFire = keyFire || bulletBtn.isPressed();

            person.onKey(lKeyLeft, lKeyRight, lKeyJump, lKeyFire);
            //hero fall 
            if (person.getBottom() < 0) {
                person.fall();
            }

        } else if (state == GAME_COMPLETED) {
            person.onKey(false, false, false, false);

        }

        buildLevel();

        if (lands.getList().size < 50) {
//        create randon
            makeMystery(true);
        }
        
//        this.timer.getLabel().setText(String.valueOf(lands.getList().size));
//        create next
//        makeMystery(false);

        for (EnumAlphabet e : this.alphabet) {
            if (this.lastLetter != null) {
                if (this.lastLetter.getNumber() + 1 == e.getNumber()) {
                    next = e;
                    break;
                }
            }
        }

//        create randon then next letter
        if (count % 100 == 0) {
            if (next == null) {
                changeAllto(false, EnumAlphabet.A);
            } else {
                changeAllto(false, next);
            }
        } else if (count2 % 40 == 0) {
            Collections.shuffle(this.alphabet);
            changeAllto(true, null);
        }

        removeLandsMysteryPassed();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        camController.camera.position.x = (int) camController.camera.position.x;
        camController.camera.position.y = (int) camController.camera.position.y;
    }

    public void toastCoin(MysteryBox mysteryBox) {

        this.lastLetter = mysteryBox.getValue();

        for (Object o : getLandsMystery(this.lastLetter.getNumber())) {
            Entity e = (Entity) o;
        }

        updatePanel();
    }

    public void addLandMysteryBox(Entity ent, boolean fix) {
        this.lands.addEntity(ent, fix);
        super.addLand(ent, fix);
    }

    public void removeLandsMysteryPassed() {
        for (Entity e : this.lands.getList()) {
            if (e instanceof MysteryBox) {
                MysteryBox m = (MysteryBox) e;
                if ((m.getX() + 650) < person.getX()) {
                    this.lands.removeEntity(m);
                }
            }
        }
    }

    public List getLandsMystery(int number) {
        List list = new ArrayList();
        for (Entity e : this.lands.getList()) {
            if (e instanceof MysteryBox) {
                MysteryBox m = (MysteryBox) e;
                if (m.getValue().getNumber() == number) {
                    list.add(e);
                }
            }
        }
        return list;
    }

//    true to randon
    public void changeAllto(boolean flag, EnumAlphabet ee) {

        for (Entity e : this.lands.getList()) {
            if (e instanceof MysteryBox) {
                MysteryBox m = (MysteryBox) e;
                if (flag) {
                    Collections.shuffle(this.alphabet);
                    ee = this.alphabet.get(0);
                }
                m.setValue(ee);
                m.getLabel().setText(m.getValue().name());
            }
        }
    }

    public void heroHitMystery(MysteryBox box) {
        if (!box.hit(this.lastLetter)) {
            showChallengeDialog();
        } else {
            this.lands.removeEntity(box);
        }
    }

    public void showChallengeDialog() {
        pauseLevel();
        challengeDialog = new ChallengeDialog();
        addOverlayChild(challengeDialog);
        centerActorXY(challengeDialog);
        challengeDialog.addListener(new MessageListener() {

            //the dialog callback
            @Override
            protected void receivedMessage(int message, Actor actor) {
                if (actor == challengeDialog) {
                    if (message == ChallengeDialog.ON_RESUME) {
                        resumeLevel();
                    } else if (message == ChallengeDialog.ON_QUIT) {
                        call(Level.COMPLETED);
                    }
                }
            }

        });
    }

    @Override
    public void pause() {
        pauseLevel();
        super.pause();
    }

    @Override
    public void resume() {

        super.resume();
    }

    @Override
    public void hide() {
        MyGame.media.stopAllMusic();
        super.hide();
    }

    //MY Gambi
    public void addOverlayChild(Actor actor) {
        super.addOverlayChild(actor);
    }

    public void centerActorXY(Actor actor) {
        super.centerActorXY(actor);
    }

}
