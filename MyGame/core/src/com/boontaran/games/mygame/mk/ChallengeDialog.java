package com.boontaran.games.mygame.mk;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.boontaran.MessageEvent;
import com.boontaran.games.mygame.MyGame;
import com.boontaran.games.mygame.ToggleButton;

/**
 *
 * @author Ecar. S. M.
 */
public class ChallengeDialog extends Group {
    // 2 mode of the dialog, quit game or resume game

    public static final int ON_RESUME = 1;
    public static final int ON_QUIT = 2;

    private int i = 1;

    //music & sound mute btn
    private ToggleButton doll_0, doll_1, doll_2, doll_3, doll_4, doll_5;
    private ImageButton shorts, shirt, tennis, vest, hat;

    public ChallengeDialog() {
        //the bg
        NinePatch patch = new NinePatch(MyGame.atlas.findRegion("dialog_bg"), 60, 60, 60, 60);
        Image bg = new Image(patch);
        bg.setSize(600, 500);
        setSize(bg.getWidth(), bg.getHeight());
        addActor(bg);

        //resume btn
//        ImageButton resumeBtn = new ImageButton(
//                new TextureRegionDrawable(MyGame.atlas.findRegion("resume_btn")),
//                new TextureRegionDrawable(MyGame.atlas.findRegion("resume_btn_down")));
//
//        addActor(resumeBtn);
//        resumeBtn.setX(getWidth() - resumeBtn.getWidth() - 40);
//        resumeBtn.setY(46);
//
//        //fire the 'resume' event
//        resumeBtn.addListener(new ClickListener() {
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                fire(new MessageEvent(ON_RESUME));
//            }
//        });

        //<editor-fold defaultstate="collapsed" desc="Dools">
        doll_5 = new ToggleButton(new Image(MyGame.atlas.findRegion("doll_5")), new Image(MyGame.atlas.findRegion("nothing")));
        addActor(doll_5);
        doll_5.setY(150);
        doll_5.setX(getWidth() / 2 - doll_5.getWidth() / 2);
        doll_5.setOn(false);
        doll_5.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
//                doll_5.setOn(false);
            }
        });

        doll_4 = new ToggleButton(new Image(MyGame.atlas.findRegion("doll_4")), new Image(MyGame.atlas.findRegion("nothing")));
        addActor(doll_4);
        doll_4.setY(150);
        doll_4.setX(getWidth() / 2 - doll_4.getWidth() / 2);
        doll_4.setOn(false);
        doll_4.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
//                doll_4.setOn(false);
            }
        });

        doll_3 = new ToggleButton(new Image(MyGame.atlas.findRegion("doll_3")), new Image(MyGame.atlas.findRegion("nothing")));
        addActor(doll_3);
        doll_3.setY(150);
        doll_3.setX(getWidth() / 2 - doll_3.getWidth() / 2);
        doll_3.setOn(false);
        doll_3.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
//                doll_3.setOn(false);
            }
        });

        doll_2 = new ToggleButton(new Image(MyGame.atlas.findRegion("doll_2")), new Image(MyGame.atlas.findRegion("nothing")));
        addActor(doll_2);
        doll_2.setY(150);
        doll_2.setX(getWidth() / 2 - doll_2.getWidth() / 2);
        doll_2.setOn(false);
        doll_2.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
//                doll_2.setOn(false);
            }
        });

        doll_1 = new ToggleButton(new Image(MyGame.atlas.findRegion("doll_1")), new Image(MyGame.atlas.findRegion("nothing")));
        addActor(doll_1);
        doll_1.setY(150);
        doll_1.setX(getWidth() / 2 - doll_1.getWidth() / 2);
        doll_1.setOn(false);
        doll_1.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
//                doll_1.setOn(false);
            }
        });

        doll_0 = new ToggleButton(new Image(MyGame.atlas.findRegion("doll_0")), new Image(MyGame.atlas.findRegion("nothing")));
        addActor(doll_0);
        doll_0.setY(150);
        doll_0.setX(getWidth() / 2 - doll_0.getWidth() / 2);
        doll_0.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
//                doll_0.setOn(false);
            }
        });
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Clothes">
        shorts = new ImageButton(new TextureRegionDrawable(MyGame.atlas.findRegion("shorts")),
                new TextureRegionDrawable(MyGame.atlas.findRegion("bullet")));
        addActor(shorts);
        shorts.setY(130);
        shorts.setX(80);
        shorts.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!order(0)) {
                    removeActor(shorts);
                    doll_0.setOn(false);
                    doll_1.setOn(true);
                    MyGame.media.playSound("coin");
                } else {
                    MyGame.media.playSound("hit");
                }
            }
        });

        shirt = new ImageButton(new TextureRegionDrawable(MyGame.atlas.findRegion("shirt")),
                new TextureRegionDrawable(MyGame.atlas.findRegion("bullet")));
        addActor(shirt);
        shirt.setY(350);
        shirt.setX(400);
        shirt.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!order(1)) {
                    removeActor(shirt);
                    doll_1.setOn(false);
                    doll_2.setOn(true);
                    MyGame.media.playSound("coin");
                } else {
                    MyGame.media.playSound("hit");
                }
            }
        });        

        tennis = new ImageButton(new TextureRegionDrawable(MyGame.atlas.findRegion("tennis")),
                new TextureRegionDrawable(MyGame.atlas.findRegion("bullet")));
        addActor(tennis);
        tennis.setY(350);
        tennis.setX(80);
        tennis.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!order(2)) {
                    removeActor(tennis);
                    doll_2.setOn(false);
                    doll_3.setOn(true);
                    MyGame.media.playSound("coin");
                } else {
                    MyGame.media.playSound("hit");
                }
            }
        });

        vest = new ImageButton(new TextureRegionDrawable(MyGame.atlas.findRegion("vest")),
                new TextureRegionDrawable(MyGame.atlas.findRegion("bullet")));
        addActor(vest);
        vest.setY(130);
        vest.setX(400);
        vest.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!order(3)) {
                    removeActor(vest);
                    doll_3.setOn(false);
                    doll_4.setOn(true);
                    MyGame.media.playSound("coin");
                } else {
                    MyGame.media.playSound("hit");
                }
            }
        });

        hat = new ImageButton(new TextureRegionDrawable(MyGame.atlas.findRegion("hat")),
                new TextureRegionDrawable(MyGame.atlas.findRegion("bullet")));
        addActor(hat);
        hat.setY(50);
        hat.setX(250);
        hat.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (!order(4)) {
                    removeActor(hat);
                    doll_4.setOn(false);
                    doll_5.setOn(true);
                    MyGame.media.playSound("coin");
                } else {
                    MyGame.media.playSound("hit");
                }
            }
        });
        //</editor-fold>

    }

    private boolean order(int i) {
        if (i + 1 == this.i) {
            this.i++;

            if (this.i > 5) {
                this.i = 1;
                fire(new MessageEvent(ON_RESUME));
            }
            return false;
        }
        return true;
    }

}
