package com.boontaran.games.mygame;

public class Settings {

	public static final float GRAVITY = -1300;
	public static final float JUMP_SPEED = 500;
//	public static final float WALK_SPEED = 1000;
        public static final float RUN_SPEED = 1500;
	
	public static final boolean PROFILE_GL = false;
	public static final boolean LOG_FPS = false;
	
}
