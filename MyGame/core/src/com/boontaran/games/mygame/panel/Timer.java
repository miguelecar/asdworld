package com.boontaran.games.mygame.panel;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.boontaran.games.mygame.MyGame;
import java.text.DecimalFormat;
import java.util.TimerTask;

public class Timer extends Group {

    private Label label;
    private double time;

    public Timer() {
        setTransform(false);
        //bullet image
        Image bullet = new Image(MyGame.atlas.findRegion("bullet_icon"));
        addActor(bullet);

        //the label showing how many bullets in stock
        LabelStyle style = new LabelStyle();
        style.font = MyGame.font1;
        style.fontColor = new Color(0xffffffff);
        label = new Label("555", style);
        setSize(label.getRight() + 30, 30);
        bullet.setY((getHeight() - bullet.getHeight()) / 2);
        label.setY((getHeight() - label.getHeight()) / 2);

        addActor(label);
        label.setX(30);
        label.setText("0");
        this.time = 0.0;
    }

    //set the label text
    public void start() {

        new java.util.Timer().schedule(new TimerTask() {
            @Override
            public void run() {
//                time += 0.01;
//                DecimalFormat df = new DecimalFormat("0.##");
//                label.setText(df.format(time));
//                System.out.println("HERE - " + df.format(time));
            }
        }, 100);
//		label.setText(String.valueOf(count));
    }

    public void pause() {

    }

    /**
     * @return the label
     */
    public Label getLabel() {
        return label;
    }

}
