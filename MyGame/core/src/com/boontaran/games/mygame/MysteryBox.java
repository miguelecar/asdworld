package com.boontaran.games.mygame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.boontaran.MessageListener;
import com.boontaran.games.platformerLib.Entity;
import com.boontaran.games.mygame.mk.ChallengeDialog;
import com.boontaran.games.mygame.mk.EnumAlphabet;

public class MysteryBox extends Entity {

    private Label label;
    public static final int EXPIRED = 1;

    //the contents, it should only one of these :
    private int numCoin, numBullets;

    private EnumAlphabet value;

    private Level level;

    private Image img, imgHit, imgInactive;
    private float hitTime;
    private boolean hasExpired;
    private static final TextureAtlas.AtlasRegion mysterybox = MyGame.atlas.findRegion("mystery_box");
    private static final TextureAtlas.AtlasRegion mysteryboxhit = MyGame.atlas.findRegion("mystery_box_hit");
    private static final TextureAtlas.AtlasRegion mysteryboxinactive = MyGame.atlas.findRegion("mystery_box_inactive");
    public MysteryBox(Level level) {
        this.level = level;
        setSize(64, 64);

        // 3 image states of the box
        img = new Image(mysterybox);
        imgHit = new Image(mysteryboxhit);
        imgInactive = new Image(mysteryboxinactive);

        setImage(img);

        //label for coin number
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = MyGame.font1;
        style.fontColor = new Color(0xffffffff);
        label = new Label("555", style);

        addActor(label);
        label.setX(20);
        label.setY(0);

        //floating
        noGravity = true;
    }

    //set the num of coin
    public void setCoin(int num) {
        numCoin = 1;
    }
    //set bullets

    public void setBullet(int numBullets) {
        this.numBullets = numBullets;
    }

    //when hero hit the box
    public boolean hit(EnumAlphabet last) {
        if (hasExpired) {
            return true;
        } else if (last == null) {
            if (getValue().getNumber() == 1) {
                level.toastCoin(this);
                setImage(imgHit);
                setExpired();
                MyGame.media.playSound("coin");
                return true;
            } else {
                MyGame.media.playSound("hit");
                return false;
            }
        } else if (last.getNumber() + 1 == getValue().getNumber()) {
            level.toastCoin(this);
            setImage(imgHit);
            setExpired();
            MyGame.media.playSound("coin");
            return true;
        } else {
            MyGame.media.playSound("hit");
            return false;
        }

        //if coin inside
//		if(numCoin > 0) {
//			level.toastCoin(this);
//			hitTime = 0.1f;
//			setImage(imgHit);
//			numCoin--;
//			
//			if(numCoin==0) {
//				setExpired();
//			}
//			return;
//		}
//		
//		//if bullets inside
//		if(numBullets>0) {
//			
//			//put bullet stock above the box
//			BulletStock stock = new BulletStock(numBullets);
//			stock.setX(getX() + (getWidth() - stock.getWidth())/2);
//			stock.setY(getTop() + stock.getHeight()/2);
//			stock.setFloating(false);
//			level.addEntity(stock);
//			
//			//little bounce
//			stock.setVY(70);
//			setExpired();
//			
//			return;
//		}
//		
//		
//		
    }

    //box has expired, turn into a metal block
    private void setExpired() {
        setImage(imgInactive);
        hasExpired = true;
    }

    @Override
    public void update(float delta) {
        super.update(delta);

        //prevent multi hit at a time, check the min elapsed time between hits
        if (hitTime > 0) {
            hitTime -= delta;
            if (hitTime <= 0) {
                setImage(img);
                if (hasExpired) {
                    setImage(imgInactive);

                }
            }
        }
    }

    /**
     * @return the value
     */
    public EnumAlphabet getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(EnumAlphabet value) {
        this.value = value;
    }

    /**
     * @return the label
     */
    public Label getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(Label label) {
        this.label = label;
    }

}
