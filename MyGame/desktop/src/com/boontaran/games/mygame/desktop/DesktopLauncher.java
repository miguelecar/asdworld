package com.boontaran.games.mygame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.boontaran.games.mygame.MyGame;

public class DesktopLauncher {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg  =new LwjglApplicationConfiguration();

		//size of game
		cfg.width = 960;
		cfg.height = 640;

		float scale = 1;
		//scaling factor
		scale = 1.2f;

		cfg.width = (int) (scale*960);
		cfg.height = (int) (scale*640);

		new LwjglApplication(new MyGame(),cfg);
	}
}
